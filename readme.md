# Demo Project

This demo project contains 3 docker containers that do the following:
 - An MQTT broker
 - A web server
 - A python script that publishes random numbers

 to run locally, install requirements:

`pip install -r requirements.txt`

then:

`docker-compose up -d`

to start the containers.

Browse to http://localhost to see the web page.

run `./consumer.py` to view the numbers being generated.