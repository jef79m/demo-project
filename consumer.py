#!/usr/bin/env python

import paho.mqtt.client as mqtt

from randomnumber import RandomNumber

topic = "jef79m"

def on_connect(client, userdata, flags, rc):
    print("Connected with result code: {}".format(rc))
    client.subscribe("{}/#".format(topic))

def on_message(client, userdata, message):
    rand = RandomNumber(json_payload=message.payload)
    print(rand)

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("127.0.0.1", 1883, 60)

client.loop_forever()

