#!/usr/bin/env python

from random import randint
from time import sleep

import paho.mqtt.client as mqtt

from randomnumber import RandomNumber

topic = "jef79m"


def on_connect(client, userdata, flags, rc):
    print("Connected with result code: {}".format(rc))


client = mqtt.Client()
client.on_connect = on_connect

max_tries = 10
try_no = 1
while try_no < max_tries:
    try:
        client.connect("mqtt", 1883, 60)
    except ConnectionRefusedError as e:
        print("Broker not found, waiting to retry in %s seconds" % try_no)
        sleep(try_no)
        pass
    else:
        break

while True:
    sleep(randint(0, 5))
    payload = RandomNumber()
    client.publish("{}/randint".format(topic), payload.as_json())
