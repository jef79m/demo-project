from random import randint
from datetime import datetime, timezone
import json
from dateutil import parser


class RandomNumber(object):
    """
    A class to represent our random number payload
    
    On intialization, a PRN is generated between 0 and 100, and a
    timestamp is recorded. A RandomNumber can be initialized with 
    known values when `json_payload` is passed as an argument (the result of .as_json())
    """

    def __init__(self, json_payload=None):
        if json_payload:
            try:
                json_payload = json_payload.decode('utf-8')  # Ensure we have a string, not bytes.
            except AttributeError:
                pass
            decoded = json.loads(json_payload)
            self.value = decoded['value']
            self.timestamp = parser.parse(decoded['timestamp'])
        else:
            self.value = randint(0, 100)  # Fairly random number
            # Store timestamp with timezoneinfo in case we need to compare among multiple sources
            self.timestamp = datetime.now(timezone.utc).astimezone()

    def as_json(self):
        data = {
            'value': self.value,
            'timestamp': self.timestamp.isoformat()  # Represent in standard way with TZ appended
        }
        return json.dumps(data)

    def __str__(self):
        return "{value}\t{timestamp}".format(**self.__dict__)
